import Head from 'next/head';
import React, {useState, Fragment} from 'react';
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_LIST, UPDATE_LIST } from "../Apollo/Queries";

export default function App () {
  const [item, setItem] = useState('');
  const { data } = useQuery(GET_LIST);
  const [add] =  useMutation(UPDATE_LIST);

  return (
        <Fragment>
          <Head>
            <title>Practice</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
          </Head>
          <div className="container mt-5">
            <h3>Enter any value:</h3>
            <input type="text" id="item" name="item" value={item} onChange={e => setItem(e.target.value)} className="form-control" />
            <button disabled={!item} onClick={()=>add({variables: {offset:item}})} className="btn btn-primary form-control mt-4">Add</button>
            <h4 className="mt-5">Values entered will be shown here:</h4>
            {data.listVal.map((added,index)=><li key={index}>{added}</li>)}
          </div> 
        </Fragment>
  );
};
