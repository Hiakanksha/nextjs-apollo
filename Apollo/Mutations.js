import { GET_LIST } from "../Apollo/Queries";

export const Mutations = {
  updateList: (_, variables, { cache }) => {
    const data = cache.readQuery({ query: GET_LIST });
    const newListValue = data.listVal.concat(variables.offset)
    cache.writeData({data: { listVal: newListValue }});
    console.log(newListValue)
  }
};
