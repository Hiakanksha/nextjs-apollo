import gql from "graphql-tag";
export const GET_LIST = gql`
  query GetListValue {
    listVal @client 
  }
`;

export const UPDATE_LIST = gql`
  mutation updateList($offset: String!) {
    updateList(offset: $offset) @client
  }
`;
