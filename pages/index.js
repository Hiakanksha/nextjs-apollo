import React, {useEffect,useState} from 'react'
import ApolloClient, { InMemoryCache } from "apollo-boost";
import {ApolloProvider} from '@apollo/react-hooks';
import {persistCache} from 'apollo-cache-persist';
import { Mutations } from "../Apollo/Mutations";
import App from '../components/App'


const cache = new InMemoryCache();
const client = new ApolloClient({
  cache,
  resolvers: {
    Mutation: {
      ...Mutations
    }
  },
});

async function setupPersistence() {
    try {
        await persistCache({
        cache: cache,
        storage: window.sessionStorage
        })
    } catch (err) {
        console.log(err)
    }
}

const defaults = { listVal: [] };
cache.writeData({ data: defaults });


const Index = () => {
    const [ hydrated, setHydrated ] = useState(false)
    useEffect(() => {
    setupPersistence()
        .finally(() => setHydrated(true))
    }, [])

    if (!hydrated)
    return <p>Loading our persisted cache...</p>


    return (
        <ApolloProvider client={client}>
            <App/>
        </ApolloProvider>
    )
}

export default Index